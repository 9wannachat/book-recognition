import { Component,OnInit } from '@angular/core';
import { NavController,AlertController,LoadingController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  public items:any;
  public name: any;
  public price:any;
  public detail:any;
  public web:any;
  myphoto: any;
  myphoto1: any
  place : string;
  link : string;

  ngOnInit(){
    this.upphoto();
  }

  constructor(public navCtrl: NavController,private alert:AlertController,private http:HTTP,private camera: Camera,private loadingCtrl:LoadingController ,private transfer: FileTransfer,) {

  }
  photo(){
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum : true
    }
    
    this.camera.getPicture(options).then((imageData) => {     
     let base64Image = 'data:image/jpeg;base64,' + imageData;  
     console.log(base64Image)   
    }, (err) => {
      console.log(err.err)
    });
  }
  
  getImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false
    }

    this.camera.getPicture(options).then((imageData) => {

      this.myphoto = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {

    });
  }

  upphoto(){
      //Show loading
      let loader = this.loadingCtrl.create({
        content: "Uploading..."
      });
      loader.present();  
  
      //create file transfer object
      const fileTransfer: FileTransferObject = this.transfer.create();
  
      //random int
      var random = Math.floor(Math.random() * 100);
  
      //option transfer
      let options: FileUploadOptions = {
        fileKey: 'file',
        fileName: "myImage_" + random + ".jpg",
        chunkedMode: false,
        httpMethod: 'post',
        mimeType: "image/jpeg",
        headers: {}
      }
  
      //file transfer action
      fileTransfer.upload(this.myphoto, 'http://192.168.43.63/ocrapi/uploadPhoto.php', options)
        .then((data) => {  
          var rawData = JSON.parse(data['response'])
          this.myphoto1 = "http://192.168.43.63/ocrapi/img/" + ((rawData['link']));
          //console.log(this.myphoto1)
          // let alert = this.alert.create({
          //   title:'Debug',
          //   message: this.myphoto1
          // });
          // alert.present();

          loader.dismiss();
  
          let loaderFinal = this.loadingCtrl.create({
            content: "กำลังประมวลผล"
          });
          loaderFinal.present();
  
          this.http.post('http:/192.168.43.63:5000/', { "link": this.myphoto1 }, { "Content-Type": "application/x-www-form-urlencoded" })
            .then(res => {
              loaderFinal.dismiss();              
              var data = JSON.parse((res.data));
              this.detail = ((data['data']['data']));
              // this.name= this.items.data[0][1]
              // this.price=this.items.data[0][3]
              // this.detail=this.items.data[0][4]
              // this.web=this.items.data[0][5]
              // console.log(JSON.stringify(this.link))
              // this.alert.create({
              //   title : "data",
              //   message : data.data["data"][0].detail
              // }).present();
              
            })
            .catch(error => {
              loaderFinal.dismiss();
              this.alert.create({
                title : "ระบบ",
                message : JSON.stringify(error)
              }).present();
              console.log(error.error)
              
            })
          
        }, (err) => {
          // console.log(err);
          // this.alert.create({
          //   title : "ผลลัพธ์",
          //   message : JSON.stringify(err)
          // }).present();
          console.log(err);
         loader.dismiss();
        });
  }
}
